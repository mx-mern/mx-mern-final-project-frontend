import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import './assets/css/style.css';
import LoginScreen from '../src/pages/LoginScreen'
import RegisterScreen from '../src/pages/RegisterScreen'
import HomeScreen from '../src/pages/HomeScreen'
import Dashboard from '../src/pages/Dashboard'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact={true} path='/' component={HomeScreen} />
        <Route path='/login' component={LoginScreen} />
        <Route path='/register' component={RegisterScreen} />
        <Route path='/dashboard' component={Dashboard} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
