
import React, { Component } from "react";
import { Dropdown, DropdownButton } from 'react-bootstrap'
import authen from '../services/authen'
import storage from '../core/storage'
import logo from "../static/images/logo.png"
import './Navbar.css';
class NavbarLogin extends Component {
    state = {
        currentUser: undefined
    };
    
    render() {

        const handleLogout = async() => {
            const response = await authen.logout()
            if (response.success)
            {
                // cookies.remove()
                storage.clear()                
                window.location.href = '/';
                
            } else {
                console.log(JSON.stringify(response))
                alert("Logout Failed")
            }
        }

        return (
            <nav className="navbar navbar-expand navbar-dark bg-dark static-top">

                <a className="navbar-brand mr-1" href="/">Our Logo</a>

                <div className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

                </div>

                <ul className="navbar-nav ml-auto ml-md-0">
                    <li className="nav-item dropdown no-arrow">
                        <DropdownButton
                            alignRight
                            title="User"
                            id="dropdown-menu-align-right"
                        >
                            <Dropdown.Item eventKey="1">Action</Dropdown.Item>
                            <Dropdown.Item eventKey="2">Another action</Dropdown.Item>
                            <Dropdown.Divider />
                            <Dropdown.Item eventKey="3"  onClick={handleLogout}>Log out</Dropdown.Item>
                        </DropdownButton>
                    </li>
                </ul>

            </nav>
        );
    }
}

export default NavbarLogin;