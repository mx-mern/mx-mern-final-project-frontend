import React, { Component } from "react";
import logo from "../static/images/logo.png"
import './Navbar.css';
class Navbar extends Component {
    state = {
        currentUser: undefined
    };
    render () {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                {/* <a className="navbar-brand" href="#">
                ThingsConnect
                </a> */}
                <img className="navbar-brand mr-auto" src={logo} alt="My Logo"/>
                {/* <div className="nav-brand navbar-toggler-icon mr-auto">My Logo</div> */}
                <div className="nav-content mr-auto mr-sm-1">
                    <a href="/"> Home  </a>
                    <a href="/"> Products  </a>
                    <a href="/"> Company  </a>
                </div>
            </nav>
        );
    }
}

export default Navbar;