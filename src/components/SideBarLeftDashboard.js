import React, { Component } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import { BrowserRouter as Router, Link } from "react-router-dom";

class SideBarLeftDashboard extends Component {
    state = {
        loading: false
    }
    render() {
        return (
            <ul className="sidebar navbar-nav">
                <li className="nav-item active">
                    <Link className="nav-link" to="/dashboard">
                        <span>Dashboard</span>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/dashboard/setup">
                        <span>Setup</span>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/group">
                        <span>Group</span>
                    </Link>
                </li>
            </ul>
        );
    }
}

export default SideBarLeftDashboard;