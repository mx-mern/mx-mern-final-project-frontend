import React, { Component } from 'react';
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

class SideBarLeftDashboard extends Component {
    state = {
        loading: false,
        show: false
    }
    render() {
        const handleShow = () => this.setState((state) => {
            return { show: !state.show }
        })
        return (
            <div className="col-2">
                {this.state.loading ? (
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>) : (
                        <div>
                            <Button variant="primary" size="md" onClick={handleShow}>
                                New Item
                            </Button>
                            <Modal show={this.state.show} onHide={handleShow}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Create Item</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form>
                                        <Form.Group as={Row} controlId="formPlaintextEmail">
                                            <Form.Label column sm="4">
                                                Item Name
                                            </Form.Label>
                                            <Col sm="8">
                                                <Form.Control type="text" placeholder="email@example.com" />
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row} controlId="formPlaintextEmail">
                                            <Form.Label column sm="4">
                                                Item ID
                                            </Form.Label>
                                            <Col sm="8">
                                                <Form.Control type="text" placeholder="email@example.com" />
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row} controlId="formPlaintextEmail">
                                            <Form.Label column sm="4">
                                                Item Type
                                            </Form.Label>
                                            <Col sm="8">
                                                <Form.Control as="select">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row} controlId="formPlaintextEmail">
                                            <Form.Label column sm="4">
                                                Item Icon
                                            </Form.Label>
                                            <Col sm="8">
                                                <Form.Control as="select">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>

                                        <Form.Group as={Row} controlId="formPlaintextPassword">
                                            <Form.Label column sm="4">
                                                Update Method
                                            </Form.Label>
                                            <Col sm="8">
                                                <Form.Control as="select">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </Form.Control>
                                            </Col>
                                        </Form.Group>
                                    </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleShow}>
                                        Close
                                </Button>
                                    <Button variant="primary" onClick={handleShow}>
                                        Create New Item
                                </Button>
                                </Modal.Footer>
                            </Modal>
                        </div>
                    )
                }
            </div>
        );
    }
}

export default SideBarLeftDashboard;