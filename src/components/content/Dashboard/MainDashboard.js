import React, { Component } from 'react';
import { Button, Accordion, Card, Modal, Row, Col, Form } from 'react-bootstrap';
import { BrowserRouter, Route, Switch, HashRouter } from 'react-router-dom';
import authen from '../../../services/authen'
import storage from '../../../core/storage'

class MainDashboard extends Component {
    state = {
        loading: false,
        show: false
    }
    render() {
        const handleShow = () => this.setState((state) => {
            return { show: !state.show }
        })
        const handleInfoShow = async () => {
            const response = await authen.getUserInfo()
            if (response.success)
            {
                if (response.data.success) {
                    alert("User Data: " + JSON.stringify(response.data.data))
                } else {
                    alert("Please Reloggin")
                    this.props.history.goBack();
                    this.props.history.push('/login');
                }
            } else {
                alert("Please Login first")
            }
        }
        const handleLogout = async() => {
            const response = await authen.logout()
            if (response.success)
            {
                // cookies.remove()
                storage.clear()
                alert("Logout succesffull")
                this.props.history.goBack();
                
            } else {
                console.log(JSON.stringify(response))
                alert("Logout Failed")
            }
        }
        return (
            <div class="container-fluid">
                <div className="row mt-2 mb-3">
                    <Col md="auto"><h2>All Items</h2></Col>
                    <Col>
                        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2"></input>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        Search
                                </button>
                                </div>
                            </div>
                        </form>
                    </Col>
                    <Col md="auto">
                        <Button variant="primary" size="md" onClick={handleShow}>
                            New Item
                        </Button>
                    </Col>
                    <Modal show={this.state.show} onHide={handleShow}>
                        <Modal.Header closeButton>
                            <Modal.Title>Create Item</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <Form.Group as={Row} controlId="formPlaintextEmail">
                                    <Form.Label column sm="4">
                                        Item Name
                                        </Form.Label>
                                    <Col sm="8">
                                        <Form.Control type="text" placeholder="email@example.com" />
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formPlaintextEmail">
                                    <Form.Label column sm="4">
                                        Item ID
                                        </Form.Label>
                                    <Col sm="8">
                                        <Form.Control type="text" placeholder="email@example.com" />
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formPlaintextEmail">
                                    <Form.Label column sm="4">
                                        Item Type
                                        </Form.Label>
                                    <Col sm="8">
                                        <Form.Control as="select">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </Form.Control>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formPlaintextEmail">
                                    <Form.Label column sm="4">
                                        Item Icon
                                        </Form.Label>
                                    <Col sm="8">
                                        <Form.Control as="select">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </Form.Control>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formPlaintextPassword">
                                    <Form.Label column sm="4">
                                        Update Method
                                        </Form.Label>
                                    <Col sm="8">
                                        <Form.Control as="select">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </Form.Control>
                                    </Col>
                                </Form.Group>
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleShow}>
                                Close
                                </Button>
                            <Button variant="primary" onClick={handleShow}>
                                Create New Item
                                </Button>
                        </Modal.Footer>
                    </Modal>
                </div>

                <Accordion defaultActiveKey="0">
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                Group 1
                                </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="0">
                            <Card.Body className="row">
                                <Card className="col-3">
                                    <Card.Img variant="top" src="holder.js/100px180" />
                                    <Card.Body>
                                        <Card.Title>Card Title</Card.Title>
                                        <Card.Text>
                                            Some quick example text to build on the card title and make up the bulk of
                                            the card's content.
                                            </Card.Text>
                                        <Button variant="primary">Go somewhere</Button>
                                    </Card.Body>
                                </Card>
                                <Card className="col-3">
                                    <Card.Img variant="top" src="holder.js/100px180" />
                                    <Card.Body>
                                        <Card.Title>Card Title</Card.Title>
                                        <Card.Text>
                                            Some quick example text to build on the card title and make up the bulk of
                                            the card's content.
                                            </Card.Text>
                                        <Button variant="primary">Go somewhere</Button>
                                    </Card.Body>
                                </Card>
                                <Card className="col-3">
                                    <Card.Img variant="top" src="holder.js/100px180" />
                                    <Card.Body>
                                        <Card.Title>Card Title</Card.Title>
                                        <Card.Text>
                                            Some quick example text to build on the card title and make up the bulk of
                                            the card's content.
                                            </Card.Text>
                                        <Button variant="primary">Go somewhere</Button>
                                    </Card.Body>
                                </Card>
                                <Card className="col-3">
                                    <Card.Img variant="top" src="holder.js/100px180" />
                                    <Card.Body>
                                        <Card.Title>Card Title</Card.Title>
                                        <Card.Text>
                                            Some quick example text to build on the card title and make up the bulk of
                                            the card's content.
                                            </Card.Text>
                                        <Button variant="primary">Go somewhere</Button>
                                    </Card.Body>
                                </Card>
                            </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                Group 2
                                </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="1">
                            <Card.Body>
                                Hello! I'm another body
                                </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="2">
                                Group 3
                                </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="2">
                            <Card.Body>
                                Hello! I'm another body
                                </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </div>
        );
    }
}

export default MainDashboard;