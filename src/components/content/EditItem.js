import React, { Component } from "react";
import { Link } from 'react-router-dom'
import axios from "axios";
import { Button, Accordion, Card, Modal, Row, Col, Form } from 'react-bootstrap';
import item from '../../services/item';

export default class EditItem extends Component {
    constructor(props) {
        super(props);

        this.onChangeDeviceId = this.onChangeDeviceId.bind(this);
        this.onChangeDeviceName = this.onChangeDeviceName.bind(this);
        this.onChangeDeviceType = this.onChangeDeviceType.bind(this);
        this.onChangeIcon = this.onChangeIcon.bind(this);
        this.onChangeUpdateMethod = this.onChangeUpdateMethod.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            deviceId: "",
            deviceName: "",
            deviceType: "",
            icon: "",
            updateMethod: "MQTT"
        };
    }

    componentDidMount() {
        let response = item.getSingleItem(this.props.match.params.id)
        .then(response => {
            let data = response.data.data
            console.log(data);
            this.setState({
                deviceId: data.device_id,
                deviceName: data.device_name,
                deviceType: data.device_type,
                icon: data.icon,
                updateMethod: data.update_method
            });
        })
        .catch(error => {
            console.log(error);
        });
            
    }

    onChangeDeviceId(e) {
        this.setState({
            deviceId: e.target.value
        })
    }

    onChangeDeviceName(e) {
        this.setState({
            deviceName: e.target.value
        })
    }

    onChangeDeviceType(e) {
        this.setState({
            deviceType: e.target.value
        })
    }

    onChangeIcon(e) {
        this.setState({
            icon: e.target.value
        })
    }

    onChangeUpdateMethod(e) {
        this.setState({
            updateMethod: e.target.value
        })
    }

    async onSubmit(e) {
        e.preventDefault();

        const newItem = {
            device_id: this.state.deviceId,
            device_name: this.state.deviceName,
            device_type: this.state.deviceType,
            icon: this.state.icon,
            icon: this.state.icon
        };

        console.log(newItem);

        await item.updateItem(this.props.match.params.id, newItem)
            .then(res => console.log(res.data));

        this.props.history.goBack();
    }

    render() {
        return (
            <div className="container-fluid">
                <h3>Edit Item</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Device Id: </label>
                        <input
                            type="text"
                            required
                            className="form-control"
                            value={this.state.deviceId}
                            onChange={this.onChangeDeviceId}
                        />
                        {/* <select ref="userInput"
                            required
                            className="form-control"
                            value={this.state.username}
                            onChange={this.onChangeDeviceId}>
                            {
                                this.state.users.map(function (user) {
                                    return <option
                                        key={user}
                                        value={user}>{user}
                                    </option>;
                                })
                            }
                        </select> */}
                    </div>
                    <div className="form-group">
                        <label>Device Name: </label>
                        <input
                            type="text"
                            required
                            className="form-control"
                            value={this.state.deviceName}
                            onChange={this.onChangeDeviceName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Device Type: </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.deviceType}
                            onChange={this.onChangeDeviceType}
                        />
                    </div>
                    <div className="form-group">
                        <label>Icon: </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.icon}
                            onChange={this.onChangeIcon}
                        />
                    </div>
                    <div className="form-group">
                        <label>Update Method: </label>
                        <select ref="userInput"
                            required
                            className="form-control"
                            value={this.state.updateMethod}
                            onChange={this.onChangeUpdateMethod}>
                                <option
                                    key="MQTT"
                                    value="MQTT">MQTT
                                </option>
                                <option
                                    key="REST"
                                    value="REST">REST
                                </option>
                            }
                        </select>
                    </div>

                    <div className="form-group">
                        <Link to={"/dashboard"}>
                            <Button
                                variant="primary"
                                size="md"
                                className="mr-3"
                                onClick={this.onSubmit}
                            >
                                Create Item
                            </Button>
                        </Link>

                        <Link to={"/dashboard"}>
                            <Button
                                variant="secondary"
                                size="md"
                            >
                                Go Back
                            </Button>
                        </Link>
                    </div>
                </form>
            </div>
        );
    }
}
