import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Accordion, Card, Modal, Row, Col, Form } from 'react-bootstrap';
import axios from 'axios';
import item from '../../services/item';

const Item = props => (
    <div className="col-4 mb-4">
        <Card className="col-md-12">
            <Card.Text>{props.item.device_id}</Card.Text>
            <Card.Text>{props.item.device_name}</Card.Text>
            <Card.Text>{props.item.device_type}</Card.Text>
            <Card.Text>{props.item.icon}</Card.Text>
            <Card.Text>{props.item.update_method}</Card.Text>
            <Link to={"/edit/" + props.item.device_id}>
                <Button variant="primary" size="md">
                    Edit Item
                </Button>
            </Link>
        </Card>
    </div>
)

export default class ItemList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            show: false
        };
    }

    handleShow() {
        this.setState((state) => {
            return { show: !state.show }
        })
    }
    
    componentDidMount() {
        item.getAllItem()
        .then(res => {
            let data = res.data.data
            console.log(data);
            this.setState({
                items: res.data.data
            });
        })
        .catch(error => {
            console.log(error);
        });
    }

    itemList() {
        console.log(this.state.items)
        return this.state.items.map(currentItem => {
            return <Item item={currentItem} key={currentItem.device_id}/>
        })
    }

    render() {
        return (
            <Card.Body className="row">
                { this.itemList() }
            </Card.Body>
        )
    }
}