import React, { Component } from 'react';
import { Button, Accordion, Card, Modal, Row, Col, Form } from 'react-bootstrap';
import { BrowserRouter, Route, Switch, HashRouter, Link } from 'react-router-dom';
import axios from 'axios';
import { host_api } from '../../config/config';
import authen from '../../services/authen'
import storage from '../../core/storage'
import ItemList from './ItemList';

class MainDashboard extends Component {

    state = {
        loading: false,
        show: false,
        items: [],
        itemRow: []
    }

    render() {
        const handleShow = () => this.setState((state) => {
            return { show: !state.show }
        })
        const handleInfoShow = async () => {
            const response = await authen.getUserInfo()
            if (response.success) {
                if (response.data.success) {
                    alert("User Data: " + JSON.stringify(response.data.data))
                } else {
                    alert("Please Reloggin")
                    window.location = '/login'
                }
            } else {
                alert("Please Login first")
            }
        }
        return (
            <div className="container-fluid">
                <div className="row mt-2 mb-3">
                    <Col md="auto"><h2>All Items</h2></Col>
                    <Col>
                        <form className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2"></input>
                                <div className="input-group-append">
                                    <button className="btn btn-primary" type="button">
                                        Search
                                </button>
                                </div>
                            </div>
                        </form>
                    </Col>
                    <Col md="auto">
                        <Link to="/create">
                            <Button variant="primary" size="md">
                                New Item
                            </Button>
                        </Link>
                    </Col>
                </div>

                <Accordion defaultActiveKey="0">
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                Group 1
                            </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="0">
                            <ItemList />
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                Group 2
                                </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="1">
                            <Card.Body>
                                Hello! I'm another body
                                </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Card.Header>
                            <Accordion.Toggle as={Button} variant="link" eventKey="2">
                                Group 3
                                </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="2">
                            <Card.Body>
                                Hello! I'm another body
                                </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </div>
        );
    }
}

export default MainDashboard;