import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Dropdown, DropdownButton, Col, Button } from 'react-bootstrap'
import storage from '../../core/storage'
import project from '../../services/project'

export default class Setup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            connectionType: "",
            mqttHost: "",
            mqttPort: 1883,
            mqttRootPath: "",
            mqttUserName: "",
            mqttPassWord: ""
        }
    }
    componentDidMount() {
        // TODO: Load project from database (call to backend)
        console.log("Setup componentDidMount")
        if (!storage.isLoggedIn()) {
            // cookies.remove()
            storage.clear()                
            window.location.href = '/';
        }
        let userInfo = storage.getUserInfo()
        console.log("set state of Setup: " + JSON.stringify(userInfo))   
        let setup = undefined
        try {            
            let setup = JSON.parse(userInfo.setup)
            this.setState(setup)
        } catch (error) {
            console.log("failed setState setup: " + userInfo.setup)
        }
    }
    handleMqttHostChanged = (event) => {
        this.setState({
            mqttHost: event.target.value,
        })
    }
    handleMqttPortChanged = (event) => {
        this.setState({
            mqttPort: event.target.value,
        })
    }
    handleMqttRootPathChanged = (event) => {
        this.setState({
            mqttRootPath: event.target.value,
        })
    }
    handleMqttUserNameChanged = (event) => {
        this.setState({
            mqttUserName: event.target.value,
        })
    }
    handleMqttPasswordChanged = (event) => {
        this.setState({
            mqttPassWord: event.target.value,
        })
    }
    handleSubmit = async (event) => {
        event.preventDefault()
        console.log(JSON.stringify(this.state))
        let projectId = storage.getUserInfo().projects
        if (projectId) {
            const response = await project.setup(projectId, this.state)
            if (response.success) {
                if (response.data.success) {
                    console.log("Save Project setup successfully: " + JSON.stringify(response.data.data))
                    alert("Save Project setup successfully");
                } else {
                    alert("Save project failed")
                }
            }
        }else {
            alert("No project found")
        }
    }

    render() {
        const handleSelect = (eventKey, event) => {
            console.log(`HandleSelect ${eventKey} with ${event}`)
            if (eventKey === "1") {
                this.setState({ connectionType: 'MQTT' })
            }
            else if (eventKey === "2") {
                this.setState({ connectionType: 'TCP' })
            }
        }
        
        return (
            <div className="container-fluid">
                <h3>Setup</h3>
                <div>id: {storage.getUserInfo().projects}</div>
                <form className="connection-form" onSubmit={this.handleSubmit}>
                <div className="row mt-2 mb-3">
                    <Col md="auto">

                        <DropdownButton
                            alignRight
                            title="Connection Type"
                            id="dropdown-menu-align-left"
                            onSelect={handleSelect}
                        >
                            <Dropdown.Item eventKey="1">MQTT</Dropdown.Item>
                            <Dropdown.Item eventKey="2">TCP</Dropdown.Item>
                        </DropdownButton>
                    </Col>
                    <Col><h3>{this.state.connectionType}</h3></Col>
                    <Col md="auto">
                        <Button type="submit">Save</Button>
                    </Col>
                </div>
                {
                    this.state.connectionType === 'MQTT' && 
                    (
                        <div className="mqtt-group">
                            <div className="form-group row mt-2 mb-3">
                                <Col md={1}><label>Host</label></Col>
                                <Col md={10}>
                                    <input type="text"
                                        value={this.state.mqttHost}
                                        onChange={this.handleMqttHostChanged}></input>
                                </Col>
                            </div>
                            <div className="form-group row mt-2 mb-3">
                                <Col md={1}>Port</Col>
                                <Col md={10}>
                                    <input type="number"
                                        value={this.state.mqttPort}
                                        onChange={this.handleMqttPortChanged}></input>
                                </Col>
                            </div>
                            <div className="form-group row mt-2 mb-3">
                                <Col md={1}>RootPath</Col>
                                <Col md={10}>
                                    <input type="text"
                                        value={this.state.mqttRootPath}
                                        onChange={this.handleMqttRootPathChanged}></input>
                                </Col>
                            </div>
                            <div className="form-group row mt-2 mb-3">
                                <Col md={1}>Username</Col>
                                <Col md={10}>
                                    <input type="text"
                                        value={this.state.mqttUserName}
                                        onChange={this.handleMqttUserNameChanged}></input>
                                </Col>
                            </div>
                            <div className="form-group row mt-2 mb-3">
                                <Col md={1}>Password</Col>
                                <Col md={10}>
                                    <input type="password"
                                        value={this.state.mqttPassWord}
                                        onChange={this.handleMqttPasswordChanged}></input>
                                </Col>
                            </div>
                        </div>
                    )
                }
                </form>
            </div>
        )
    }
}