export const host_api = "http://localhost:8000"
export default {
    rootPath: process.env.REACT_APP_STATE === 'localhost'
                ? host_api
                :''
};
