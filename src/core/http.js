import axios from 'axios';
import config from '../config/config';
import storage from './storage'

export default {
  async http(method, url, payload, cookie = false, options = {}, responseType, requestConfig = {}) {
    try {
      const headerConfig = {}
      // const config = Object.assign({
      //   'access_token': env.ACCESS_TOKEN,
      //   'user_token': token ? localStorage.getItem("token") || storage.getUser().token : ''
      // }, options)

      const axiosData = Object.assign({
        method: method,
        baseURL: config.rootPath,
        url: url,
        withCredentials: cookie,
        headers: headerConfig        
      }, requestConfig)

      if (payload) {
        axiosData.data = payload;
      }
      
      if (responseType) {
        axiosData.responseType = responseType;
      }

      const response = await axios(axiosData)
      console.log(response, JSON.stringify(response))
      return {
        success: true,
        message: 'Success',
        ...response
      }
    } catch (err) {
      if (err.response && err.response.status === 401) {
        // await store.dispatch('processLogout')
        window.location.href = '/login'
        storage.clear()
        return;
      }
      return {
        success: false,
        error: true,
        code: err.response ? err.response.status : err.message === 'Network Error' ? 407 : 520,
        message: err.response && err.response.data && err.response.data.error ? err.response.data.error : null
      }
    }
  },

  async get(url, cookie = true, options = {}, responseType, requestConfig = {}) {
    return await this.http('get', url, null, cookie, options, responseType, requestConfig)
  },

  async post(url, payload, cookie = true, options = {}, requestConfig = {}) {
    return await this.http('post', url, payload, cookie, options, requestConfig)
  },

  async put(url, payload, cookie = true, options = {}, requestConfig = {}) {
    return await this.http('put', url, payload, cookie, options, requestConfig)
  },

  async delete(url, payload, cookie = true, options = {}, requestConfig = {}) {
    return await this.http('delete', url, payload, cookie, options, requestConfig)
  }
}