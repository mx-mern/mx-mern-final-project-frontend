export default {
    clear() {
        localStorage.clear()  
    },
    saveLoginData(data) {
        console.log(data)
        localStorage.setItem('name', data.name)
        localStorage.setItem('email', data.email)
        localStorage.setItem('projects', data.projects)
    },
    saveProjectSetup(data) {
        console.log(`project setup: ${data}`)
        localStorage.setItem('setup', data)
    },
    isLoggedIn(){
        const email = localStorage.getItem('email')
        if (email) {
            return true
        } else {
            return false
        }
    },
    getUserInfo() {
        if (this.isLoggedIn()) {
            return {
                name: localStorage.getItem('name'),
                email: localStorage.getItem('email'),
                projects: localStorage.getItem('projects'),
                setup: localStorage.getItem('setup')
            }
        } else {
            return null
        }
    }
}