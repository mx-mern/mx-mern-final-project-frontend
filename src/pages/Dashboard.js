import React, { Component } from 'react';
import NavBarLogin from '../components/NavBarLogin'
import SideBarLeftDashboard from '../components/SideBarLeftDashboard'
import MainDashboard from '../components/content/MainDashboard'
import EditItem from '../components/content/EditItem'
import CreateItem from '../components/content/CreateItem'
import Setup from '../components/content/Setup'
import { Button, Accordion, Card, Modal, Row, Col, Form } from 'react-bootstrap';
import { BrowserRouter, Route, Switch, HashRouter } from 'react-router-dom';
import storage from '../core/storage'
import project from '../services/project'

class Dashboard extends Component {
    state = {
        loading: true
    }
    async componentDidMount() {
        // TODO: Load project from database (call to backend)
        if (this.state.loading) {
            let userInfo = storage.getUserInfo()
            if (userInfo) {
                this.setState({loading: false})
                // get project info from userInfo projects 
                try {
                    let projectID = storage.getUserInfo().projects
                    const response = await project.info(projectID)
                    console.log('response of project ' +projectID + ': ' + JSON.stringify(response))
                    if (response.success)
                    {
                        if (response.data.success)
                        {
                            console.log('Project data: ' + JSON.stringify(response.data.data))
                            storage.saveProjectSetup(response.data.data.setup)
                            this.setState({loading: false})
                        } else {
                            alert("No Project Info found!!!")
                        }
                    } else {
                        alert("Get Project Info Failed")
                    }
                } catch (error) {
                    console.error(error)
                }
            }
        }
    }
    render() {
        return (
            <div className="w-100 h-100">
                <NavBarLogin />
                <div id="wrapper">
                    <SideBarLeftDashboard />

                    <div id="content-wrapper">
                        <Switch>
                            <Route exact path='/dashboard' component={MainDashboard} />
                            <Route path="/edit/:id" component={EditItem} />
                            <Route path="/create" component={CreateItem} />
                            <Route exact path={'/dashboard/setup'} component={Setup} />
                        </Switch>
                    </div>
                        {/* group loop by group */}
                        {/* item loop by groups data */}
                        {/* main content here | how to route by page? */}
                    {/* <SideBarRightDashboard /> */}
                </div>
            </div>
        );
    }
}

export default Dashboard;