import React, { Component } from 'react';
import Navbar from '../components/Navbar'
import background from "../static/images/iot_platform.jpg"
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { Link } from 'react-router-dom'
import storage from '../core/storage'


class HomeScreen extends Component {
    state = {
        loading: false
    }
    componentDidMount()
    {
        if (storage.isLoggedIn())
        {
            this.props.history.push('/dashboard');
        }
    }
    render() {
        return (
            <div className="container">
                <Navbar/>
                <div className="row mt-3">
                    <div className="col-3">
                        {this.state.loading ? (
                            <div className="spinner-border" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>) : (
                            <ButtonGroup vertical className="d-flex">
                                <Button variant="primary" className="btn-block" size="sm" onClick={()=> {this.props.history.push('/login')}}>
                                    Sign in
                                </Button>
                                <Button variant="primary" className="mt-1" size="sm" onClick={()=> {this.props.history.push('/register')}}>
                                    Sign up
                                </Button>
                                <Button variant="primary" className="mt-1" size="sm">Documentation</Button>
                            </ButtonGroup>
                            )
                        }
                    </div>
                    <div className="col-9 text-right">
                        <img className="img-fluid w-100" src={background}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default HomeScreen;