import React, { Component } from 'react'
import Navbar from '../components/Navbar'
// import config from '../config/config'
import authen from '../services/authen'
import storage from '../core/storage'

class LoginScreen extends Component {
    state = {
        email: '',
        password: '',
        errorMessage: '',
        loading: false,
    }

    handleEmailChange = (event) => {
        this.setState({
            email: event.target.value,
        })
    }

    handlePasswordChange = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    handleSubmit = async (event) => {
        event.preventDefault()
        // validate
        if (!this.state.email) {
            this.setState({
                errorMessage: 'Please input email',
            })
        } else if (!this.state.password) {
            this.setState({
                errorMessage: 'Please input password',
            })
        } else {
            this.setState({
                errorMessage: '',
                loading: true,
            })
            try {
                let data = {
                    email: this.state.email,
                    password: this.state.password
                }
                const response = await authen.login(data)// await axios.post(config.rootPath + '/user/login', data)
                console.log('response of login: ' + JSON.stringify(response))
                if (response.success)
                {
                    if (response.data.success)
                    {
                        storage.clear()
                        storage.saveLoginData(response.data.data)
                        this.props.history.push('/dashboard')
                    } else {
                        this.setState({
                            errorMessage: response.data.message,
                            loading: false,
                        })
                        alert("Email or password is not correct, errorMessage: " + this.state.errorMessage)
                    }
                } else {
                    alert("Login Failed")
                }
            } catch (error) {
                console.error(error)
            }
        }
    }

    render() {
        return (
            <div className="container">
                <Navbar />
                <div className="row mt-3">
                    <div className="col-3">
                        <form className="login-form" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="inputEmail">Email address </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    placeholder="Ex: tienthinh2011@gmail.com"
                                    value={this.state.email}
                                    onChange={this.handleEmailChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputPassword">Password </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Input password"
                                    value={this.state.password}
                                    onChange={this.handlePasswordChange}
                                />
                            </div>
                            {this.state.loading ? (
                                <div className="spinner-border" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                            ) : (
                                    <button type="submit" className="btn btn-primary">
                                        Sign In
                                </button>
                                )}
                        </form>
                    </div>
                </div>

            </div>

        )
    }
}
export default LoginScreen;