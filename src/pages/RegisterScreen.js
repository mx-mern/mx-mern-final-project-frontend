import React, { Component } from 'react'
import Navbar from '../components/Navbar'
import authen from '../services/authen'

class RegisterScreen extends Component {
    state = {
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
        errorMessage: '',
        loading: false,
    }

    handleUserNameChange = (event) => {
        this.setState({
            username: event.target.value
        })
    }

    handleEmailChange = (event) => {
        this.setState({
            email: event.target.value
        })
    }

    handlePasswordChange = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    handleConfirmPasswordChange = (event) => {
        this.setState({
            confirmPassword: event.target.value
        })
    }

    handleSubmit = async (event) => {
        event.preventDefault()
        if (!this.state.email || !this.state.username || !this.state.password || !this.state.confirmPassword) {
            alert("Please fill all fields, email: " + this.state.email
                + " name: " + this.state.username
                + " password: " + this.state.password
                + "confirmPassword: " + this.state.confirmPassword)
        } else {
            if (this.state.password != this.state.confirmPassword) {
                alert("Password is not matched")
            } else {
                // alert("Email Address is registered")
                const response = await authen.register({
                    name: this.state.username,
                    email: this.state.email,
                    password: this.state.password
                })
                if (response.success)
                {
                    if (response.data.success)
                    {
                        alert("Registered successfully! Please login to continue")
                        this.props.history.goBack();
                        this.props.history.push('/login')
                    } else {
                        this.setState({
                            errorMessage: response.data.message,
                            loading: false,
                        })
                        alert("Register Failed, errorMessage: " + this.state.errorMessage)
                    }
                } else {
                    alert("Register Failed")
                }
            }
        }
    }

    handleConfirmPasswordChange = (event) => {
        this.setState({
            confirmPassword: event.target.value
        })
    }

    render() {
        return (
            <div className="container">
                <Navbar />
                <div className="row mt-3">
                    <div className="col-3">
                        <form className="register-form" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="name">User Name </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Ex: Nguyen Tien Thinh"
                                    value={this.state.username}
                                    onChange={this.handleUserNameChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputEmail">Email address </label>
                                <input
                                    type="email"
                                    className="form-control"
                                    placeholder="Ex: tienthinh2011@gmail.com"
                                    value={this.state.email}
                                    onChange={this.handleEmailChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputPassword">Password </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Input password"
                                    value={this.state.password}
                                    onChange={this.handlePasswordChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputPassword2">Password </label>
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Confirm password"
                                    value={this.state.confirmPassword}
                                    onChange={this.handleConfirmPasswordChange}
                                />
                            </div>
                            {this.state.loading ? (
                                <div className="spinner-border" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                            ) : (
                                    <button type="submit" className="btn btn-primary">
                                        Register
                                </button>
                                )}
                        </form>
                    </div>
                </div>

            </div>

        )
    }
}
export default RegisterScreen;