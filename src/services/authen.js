import http from '../core/http';

export default {
    async register(payload) {
        console.log('Register user with payload' + JSON.stringify(payload))
        return await http.post('/user/register', payload)
    },
    async login(payload) {
        console.log('Authen: /user/login, payload: ' + JSON.stringify(payload))
        return await http.post('/user/login', payload)
    },
    async getUserInfo() {
        return await http.get('/user/info')
    },
    async logout() {
        return await http.post('/user/logout', null)
    }
}