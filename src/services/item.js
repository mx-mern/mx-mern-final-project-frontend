import http from '../core/http';

export default {
    async getAllItem() {
        // console.log('Register user with payload' + JSON.stringify(payload))
        return await http.get('/item')
    },
    async getSingleItem(id) {
        return await http.get('/item/'+id)
    },
    async updateItem(id, payload) {
        return await http.put('/item/'+id, payload)
    },
    async createItem(payload) {
        return await http.post('/item', payload)
    },
    async deleteItem(id) {
        return await http.delete('/item/'+id)
    }
}