import http from '../core/http';
import Setup from '../components/content/Setup';

export default {
    async info(projectId) {
        console.log('Get Project Info by ID' + projectId)
        let requestConfig = {
            params: {
              id: projectId
            }
        }
        return await http.get('/project/info', true, {}, null, requestConfig)
    }, 
    async setup(projectId, setup) {
        console.log("Change setup of project " + projectId + ": " + setup)
        return await http.post('/project/setup', {id: projectId, setup: setup})
    }
}